package com.test;

class Exp1
{
	int x = 20;
	double y = 45.56;
	char j = 'A';
	boolean k = false;

	public void disp()
	{
	  int a = 20;

	  System.out.println(a);

	  System.out.println(x);
	  System.out.println(y);
	  System.out.println(j);
	  System.out.println(k);

	}

	public static void main(String[] args)
	{
	  Exp1 obj = new Exp1();

	  obj.disp();

	  System.out.println("From main method");
	}

}